package com.devcamp.animalapi.service;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.devcamp.animalapi.model.Cat;


@Service
public class CatService {
     // khởi tạo 2 đối tượng con mèo 
     Cat cat1 = new Cat("meo 1");
     Cat cat2 = new Cat("meo 2");
     Cat cat3 = new Cat("meo 3");

     // phương thức trả về tất cả các con mèo được khoi tạo
     public ArrayList<Cat> getCatList() {
         ArrayList<Cat> allCatList = new ArrayList<Cat>();
         allCatList.add(cat1);
         allCatList.add(cat2);
         allCatList.add(cat3);
         return allCatList;
 
     }
}
