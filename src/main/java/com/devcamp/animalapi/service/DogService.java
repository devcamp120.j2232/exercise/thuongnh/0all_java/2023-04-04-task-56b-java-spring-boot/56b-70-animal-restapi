package com.devcamp.animalapi.service;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.devcamp.animalapi.model.Dog;

@Service
public class DogService {

    Dog dog1 = new Dog("cho 1");
    Dog dog2 = new Dog("cho 2");
    Dog dog3 = new Dog("cho 3");

    // phương thức trả về tất cả các chú chó được khoi tạo
    public ArrayList<Dog> getDogList() {
        ArrayList<Dog> allDogList = new ArrayList<Dog>();
        allDogList.add(dog1);
        allDogList.add(dog2);
        allDogList.add(dog3);
        return allDogList;

    }

}
