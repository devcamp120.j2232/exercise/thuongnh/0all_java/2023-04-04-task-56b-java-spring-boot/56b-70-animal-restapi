package com.devcamp.animalapi.controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import com.devcamp.animalapi.model.Dog;
import com.devcamp.animalapi.service.DogService;

@RestController
@CrossOrigin
public class DogController {

    @Autowired
    private DogService dogService;

    @GetMapping("/dogs")
    public ArrayList<Dog> getCatAll() {
        return dogService.getDogList();
    }

}
