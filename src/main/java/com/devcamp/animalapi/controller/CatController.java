package com.devcamp.animalapi.controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.animalapi.model.Cat;
import com.devcamp.animalapi.service.CatService;

@RestController
@CrossOrigin
public class CatController {
    

    @Autowired
    private CatService cat1Service;

    @GetMapping("/cats")
    public ArrayList<Cat> getCatAll(){
        return cat1Service.getCatList();
    }


}
